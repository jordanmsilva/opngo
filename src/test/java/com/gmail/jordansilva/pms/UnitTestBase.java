package com.gmail.jordansilva.pms;

import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.fail;

import java.lang.reflect.Field;

import org.easymock.EasyMockRunner;
import org.easymock.Mock;
import org.junit.runner.RunWith;

@RunWith(EasyMockRunner.class)
public abstract class UnitTestBase {

    /**
     * Invokes EasyMock.replay() to all fields annotated with @Mock plus the objects passed as parameter.
     * 
     * @param mocks that are not annotated with @Mock.
     */
    protected void replayAll(Object... mocks) {
        Field[] declaredFields = getClass().getDeclaredFields();

        for (Field field : declaredFields) {
            if (field.isAnnotationPresent(Mock.class)) {
                try {
                    field.setAccessible(true);
                    replay(field.get(this));
                } catch (IllegalArgumentException | IllegalAccessException e) {
                    e.printStackTrace();
                    fail("Error replaying field.");
                }
            }
        }

        if (mocks != null) {
            for (Object object : mocks) {
                replay(object);
            }
        }
    }

    /**
     * Invokes EasyMock.verify() to all fields annotated with @Mock plus the objects passed as parameter.
     * 
     * @param mocks that are not annotated with @Mock.
     */
    protected void verifyAll(Object... mocks) {
        Field[] declaredFields = getClass().getDeclaredFields();

        for (Field field : declaredFields) {
            if (field.isAnnotationPresent(Mock.class)) {
                try {
                    field.setAccessible(true);
                    verify(field.get(this));
                } catch (IllegalArgumentException | IllegalAccessException e) {
                    e.printStackTrace();
                    fail("Error verifing field.");
                }
            }
        }

        if (mocks != null) {
            for (Object object : mocks) {
                verify(object);
            }
        }
    }
}
