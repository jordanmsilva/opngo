package com.gmail.jordansilva.pms;

import static java.lang.String.format;
import static javax.ws.rs.client.Entity.entity;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

import javax.inject.Inject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
public abstract class ResourceIntegrationTestBase {

    private static final String SERVER_URL_PATTERN = "http://localhost:%s/";

    @Inject
    private Client client;

    @LocalServerPort
    int port;

    /*
     * POST
     */

    public Response post(String url) {
        return buildRequest(url).post(null);
    }

    public <E, T> T post(String url, E element, Class<T> clazz) {
        return buildRequest(url).post(entity(element, APPLICATION_JSON), clazz);
    }

    public <E> Response post(String url, E element) {
        return buildRequest(url).post(entity(element, APPLICATION_JSON));
    }

    private Builder buildRequest(String url) {
        return getTarget(url).request().accept(APPLICATION_JSON);

    }

    private WebTarget getTarget(String url) {
        return client.target(getFullURL(url));
    }

    private String getFullURL(String path) {
        return format(SERVER_URL_PATTERN, port) + path;
    }
}
