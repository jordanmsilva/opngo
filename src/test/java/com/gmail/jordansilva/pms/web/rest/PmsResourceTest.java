package com.gmail.jordansilva.pms.web.rest;

import static com.gmail.jordansilva.pms.test.ParkingLotTestUtils.PARKING_LOT_ID_ACTIVE;
import static com.gmail.jordansilva.pms.test.ParkingLotTestUtils.PARKING_LOT_ID_DOESNT_EXIST;
import static com.gmail.jordansilva.pms.test.ParkingLotTestUtils.PARKING_LOT_ID_INACTIVE;
import static com.gmail.jordansilva.pms.test.VehicleDTOTestUtils.VEHICLE_DOESNT_EXIST;
import static com.gmail.jordansilva.pms.test.VehicleDTOTestUtils.VEHICLE_NO_OWNER;
import static com.gmail.jordansilva.pms.test.VehicleDTOTestUtils.VEHICLE_OK;
import static com.gmail.jordansilva.pms.test.VehicleDTOTestUtils.VEHICLE_WITHOUT_PLATE_NUMBER;
import static java.lang.String.format;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.FORBIDDEN;
import static javax.ws.rs.core.Response.Status.OK;
import static org.assertj.core.api.Assertions.assertThat;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.junit.Test;

import com.gmail.jordansilva.pms.ResourceIntegrationTestBase;
import com.gmail.jordansilva.pms.application.model.VehicleDTO;
import com.gmail.jordansilva.pms.web.rest.model.ParkingStatus;

public class PmsResourceTest extends ResourceIntegrationTestBase {

    private static final String START_SESSION_PATH = "pms/v1/assets/%s/sessions";
    private static final String STOP_SESSION_PATH = "pms/v1/assets/%s/vehicle/%s/session";

    private static final ParkingStatus PARKING_STATUS_STOPPED = new ParkingStatus(ParkingStatus.Status.STOPPED);

    @Test
    public void startSessionShouldReturnOkResponseWhenVehicleUserAndParkingLotAreOk() {
        testStartSession(VEHICLE_OK, PARKING_LOT_ID_ACTIVE, OK, null);

        // Cleanup
        stopParkingSession(VEHICLE_OK, PARKING_LOT_ID_ACTIVE);
    }

    @Test
    public void startSessionShouldReturnForbidenWhenParkingLotDesntExist() {
        testStartSession(VEHICLE_OK, PARKING_LOT_ID_DOESNT_EXIST, FORBIDDEN, null);
    }

    @Test
    public void startSessionShouldReturnForbidenWhenParkingLotIsNotActive() {
        testStartSession(VEHICLE_OK, PARKING_LOT_ID_INACTIVE, FORBIDDEN, null);
    }

    @Test
    public void startSessionShouldReturnForbidenWhenVehicleDoesNotExist() {
        testStartSession(VEHICLE_DOESNT_EXIST, PARKING_LOT_ID_ACTIVE, FORBIDDEN, null);
    }

    @Test
    public void startSessionShouldReturnForbidenWhenVehicleHasNoOwner() {
        testStartSession(VEHICLE_NO_OWNER, PARKING_LOT_ID_ACTIVE, FORBIDDEN, null);
    }

    @Test
    public void startSessionShouldReturnBadRequestWhenNoVehicleIsSentOnTheRequest() {
        String expectedBodyMessage = "License plate number is required";
        testStartSession(null, PARKING_LOT_ID_ACTIVE, BAD_REQUEST, expectedBodyMessage);
    }

    @Test
    public void startSessionShouldReturnBadRequestWhenVehicleHasNoLicensePlateNumber() {
        String expectedBodyMessage = "License plate number is required";
        testStartSession(VEHICLE_WITHOUT_PLATE_NUMBER, PARKING_LOT_ID_ACTIVE, BAD_REQUEST, expectedBodyMessage);
    }

    @Test
    public void stopSessionShouldReturnOkResponseWhenVehicleHasActiveParking() {
        testStartSession(VEHICLE_OK, PARKING_LOT_ID_ACTIVE, OK, null);
        testStopSession(VEHICLE_OK, PARKING_LOT_ID_ACTIVE, PARKING_STATUS_STOPPED, OK, null);
    }

    @Test
    public void stopParkingShouldReturnForbiddenWhenVehicleHasNoActiveParking() {
        testStopSession(VEHICLE_OK, PARKING_LOT_ID_ACTIVE, PARKING_STATUS_STOPPED, FORBIDDEN, null);
    }

    @Test
    public void stopParkingShouldReturnForbiddenWhenVehicleIsNotRegistered() {
        testStopSession(VEHICLE_DOESNT_EXIST, PARKING_LOT_ID_ACTIVE, PARKING_STATUS_STOPPED, FORBIDDEN, null);
    }

    @Test
    public void stopParkingShouldReturnBadRequestWhenNoStatusIsSent() {
        testStopSession(VEHICLE_OK, PARKING_LOT_ID_ACTIVE, null, Status.BAD_REQUEST,
                "Invalid status. Status must be STOPPED");
    }

    @Test
    public void stopParkingShouldReturnBadRequestWhenStatusIsDifferentOfStopped() {
        ParkingStatus parkingStatus = new ParkingStatus();
        testStopSession(VEHICLE_OK, PARKING_LOT_ID_ACTIVE, parkingStatus, Status.BAD_REQUEST,
                "Invalid status. Status must be STOPPED");
    }

    private void testStartSession(VehicleDTO vehicle, Long parkingLotId, Status status, Object entity) {
        String startSessionPath = format(START_SESSION_PATH, parkingLotId);
        doPostTest(startSessionPath, vehicle, status, entity);
    }

    private void testStopSession(VehicleDTO vehicle, Long parkingLotId, ParkingStatus parkingStatus,
            Status responseStatus, Object entity) {
        String stopSessionPath = format(STOP_SESSION_PATH, parkingLotId, vehicle.getLicensePlateNumber());
        doPostTest(stopSessionPath, parkingStatus, responseStatus, entity);
    }

    private void doPostTest(String path, Object requestPayload, Status responseStatus, Object entity) {

        @SuppressWarnings("resource")
        Response response = post(path, requestPayload);

        assertThat(response.getStatus()).isEqualTo(responseStatus.getStatusCode());

        if (entity != null) {
            assertThat(response.readEntity(entity.getClass())).isEqualTo(entity);
        }
    }

    private void stopParkingSession(VehicleDTO vehicle, Long parkingLotId) {
        testStopSession(vehicle, parkingLotId, PARKING_STATUS_STOPPED, OK, null);
    }
}
