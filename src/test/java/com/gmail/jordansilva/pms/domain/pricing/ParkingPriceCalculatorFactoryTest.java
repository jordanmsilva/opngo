package com.gmail.jordansilva.pms.domain.pricing;

import static org.assertj.core.api.Assertions.assertThat;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;

import org.easymock.EasyMockRunner;
import org.easymock.Mock;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(EasyMockRunner.class)
public class ParkingPriceCalculatorFactoryTest {

    @Mock
    private StandardParkingPriceCalculator standardCalculator;

    private ParkingPriceCalculatorFactory factory;

    @Before
    public void instanciateTestSubject() {
        factory = new ParkingPriceCalculatorFactory(standardCalculator);
    }

    @Test
    public void getCalculatorShouldReturnStandardParkingPriceCalculator() {
        replay(standardCalculator);

        ParkingPriceCalculator calculator = factory.getCalculator();

        verify(standardCalculator);

        assertThat(calculator).isInstanceOf(StandardParkingPriceCalculator.class);
    }
}
