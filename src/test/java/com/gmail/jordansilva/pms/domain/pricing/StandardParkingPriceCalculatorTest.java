package com.gmail.jordansilva.pms.domain.pricing;

import static java.time.Instant.now;
import static java.time.temporal.ChronoUnit.MINUTES;
import static org.assertj.core.api.Assertions.assertThat;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;

import java.time.Instant;

import org.easymock.EasyMockRunner;
import org.easymock.Mock;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.gmail.jordansilva.pms.infrastructure.domain.config.StandardParkingPriceCalculatorConfig;

@RunWith(EasyMockRunner.class)
public class StandardParkingPriceCalculatorTest {

    private static final int TIME_UNIT_IN_MINUTES = 15;

    @Mock
    private StandardParkingPriceCalculatorConfig config;

    private StandardParkingPriceCalculator calculator;

    @Before
    public void instanciateTestSubject() {
        calculator = new StandardParkingPriceCalculator(config);
    }

    @Test
    public void calculateWithoutExtraSeconds() {
        double unitPrice = 10.0;
        int fullTimeUnits = 3;
        int extraSeconds = 0;
        double expectedTotalPrice = 30.0;

        testCalculate(unitPrice, fullTimeUnits, extraSeconds, expectedTotalPrice);
    }

    @Test
    public void calculateWithOneExtraSeconds() {
        double unitPrice = 11.0;
        int fullTimeUnits = 8;
        int extraSeconds = 1;
        double expectedTotalPrice = 99.0;

        testCalculate(unitPrice, fullTimeUnits, extraSeconds, expectedTotalPrice);
    }

    @Test
    public void calculateWithOneSecondToCompleteNextFullTime() {
        double unitPrice = 5.99;
        int fullTimeUnits = 1;
        int extraSeconds = (TIME_UNIT_IN_MINUTES * 60) - 1;
        double expectedTotalPrice = 11.98;

        testCalculate(unitPrice, fullTimeUnits, extraSeconds, expectedTotalPrice);
    }

    @Test
    public void calculateWithoutCompletingFirst15Minutes() {
        double unitPrice = 5.50;
        int fullTimeUnits = 0;
        int extraSeconds = (TIME_UNIT_IN_MINUTES * 60) / 2;
        double expectedTotalPrice = 5.50;

        testCalculate(unitPrice, fullTimeUnits, extraSeconds, expectedTotalPrice);
    }

    private void testCalculate(double unitPrice, int fullTimeUnits, int extraSeconds, double expectedTotalPrice) {
        Instant start = now();
        Instant end = start.plus(fullTimeUnits * TIME_UNIT_IN_MINUTES, MINUTES).plusSeconds(extraSeconds);

        expect(config.getUnitPrice()).andReturn(unitPrice);

        replay(config);

        double totalPrice = calculator.calculate(start, end);

        verify(config);

        assertThat(totalPrice).isEqualTo(expectedTotalPrice);
    }

}
