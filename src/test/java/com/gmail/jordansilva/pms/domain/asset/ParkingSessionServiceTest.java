package com.gmail.jordansilva.pms.domain.asset;

import static java.time.Instant.now;
import static org.assertj.core.api.Assertions.assertThat;
import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.createStrictMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.mock;
import static org.junit.Assert.fail;

import java.time.Instant;

import org.easymock.Capture;
import org.easymock.EasyMock;
import org.easymock.Mock;
import org.junit.Before;
import org.junit.Test;

import com.gmail.jordansilva.pms.UnitTestBase;
import com.gmail.jordansilva.pms.domain.asset.model.ParkingSession;
import com.gmail.jordansilva.pms.domain.pricing.ParkingPriceCalculator;
import com.gmail.jordansilva.pms.domain.pricing.ParkingPriceCalculatorFactory;
import com.gmail.jordansilva.pms.domain.user.model.User;
import com.gmail.jordansilva.pms.domain.user.model.Vehicle;
import com.gmail.jordansilva.pms.infrastructure.repositories.ParkingSessionRepository;

public class ParkingSessionServiceTest extends UnitTestBase {

    @Mock
    private ParkingSessionRepository parkingSessionRepository;

    @Mock
    private ParkingPriceCalculatorFactory priceCalculatorFactory;

    private ParkingSessionService parkingSessionService;

    @Before
    public void instanciateTestSubject() {
        parkingSessionService = new ParkingSessionService(priceCalculatorFactory, parkingSessionRepository);
    }

    @Test
    public void startShouldCreateNewParkingSessionWhenPassingValidVehicle() {
        Instant requestTime = now();

        Vehicle vehicle = new Vehicle("123-ABC");
        User user = mock(User.class);
        vehicle.setOwner(user);
        Capture<ParkingSession> parkingSessionCapture = EasyMock.<ParkingSession>newCapture();

        ParkingSession expectedParkingSession = mock(ParkingSession.class);
        expect(parkingSessionRepository.save(capture(parkingSessionCapture))).andReturn(expectedParkingSession);

        replayAll(user, expectedParkingSession);

        ParkingSession session = parkingSessionService.start(vehicle);

        verifyAll(user, expectedParkingSession);

        assertThat(session).isSameAs(expectedParkingSession);

        // Assert that persisted object has correct values
        ParkingSession persistedSession = parkingSessionCapture.getValue();
        assertThat(persistedSession).isNotNull();
        assertThat(persistedSession.getVehicle()).isEqualTo(vehicle);
        assertThat(persistedSession.getStart()).isAfterOrEqualTo(requestTime).isBeforeOrEqualTo(now());
        assertThat(persistedSession.getEnd()).isNull();
        assertThat(persistedSession.getPrice()).isNull();
    }

    @Test
    public void startShouldShouldReturnNullWhenVehicleHasNoOwner() {
        Vehicle vehicle = new Vehicle("123-ABC");

        replayAll();

        ParkingSession session = parkingSessionService.start(vehicle);

        verifyAll();

        assertThat(session).isNull();
    }

    @Test
    public void startShouldThrowNullPointerExceptionWhenPassingNullVehicle() {
        replayAll();

        try {
            parkingSessionService.start(null);
            fail("Exception expected: Vehicle parameter cannot be null.");
        } catch (NullPointerException e) {
            assertThat(e.getMessage()).isEqualTo("vehicle is marked @NonNull but is null");
        }

        verifyAll();
    }

    @Test
    public void stopShouldReturnTrueWhenThereIsActiveSessionForVehicleAndSessionIsEnded() {
        testSuccessOrFailOnSave(true);
    }

    @Test
    public void stopShouldReturnFalseWhenThereIsNoActiveSessionForVehicle() {
        Vehicle vehicle = mock(Vehicle.class);

        expect(parkingSessionRepository.findOneByVehicleAndEndIsNull(vehicle)).andReturn(null);

        replayAll(vehicle);

        boolean stoped = parkingSessionService.stop(vehicle);

        verifyAll(vehicle);

        assertThat(stoped).isFalse();
    }

    @Test
    public void stopShouldReturnFalseWhenDatabaseErrorWhileSavingEndedSession() {
        testSuccessOrFailOnSave(false);
    }

    public void testSuccessOrFailOnSave(boolean result) {
        Instant requestTime = now();
        Instant start = requestTime.minusSeconds(12);
        Instant end = requestTime.plusSeconds(23);

        Vehicle vehicle = mock(Vehicle.class);
        ParkingSession parkingSession = createStrictMock(ParkingSession.class);
        Capture<Instant> endCapture = EasyMock.<Instant>newCapture();
        ParkingPriceCalculator calculator = mock(ParkingPriceCalculator.class);
        Double totalPrice = 12.34;

        expect(parkingSessionRepository.findOneByVehicleAndEndIsNull(vehicle)).andReturn(parkingSession);
        parkingSession.setEnd(capture(endCapture));
        expect(parkingSession.getStart()).andReturn(start);
        expect(parkingSession.getEnd()).andReturn(end);
        expect(priceCalculatorFactory.getCalculator()).andReturn(calculator);
        expect(calculator.calculate(start, end)).andReturn(totalPrice);
        parkingSession.setPrice(totalPrice);

        if (result) {
            // should do nothing with the returned element
            expect(parkingSessionRepository.save(parkingSession)).andReturn(null);
        } else {
            expect(parkingSessionRepository.save(parkingSession)).andThrow(new RuntimeException());
        }

        replayAll(vehicle, parkingSession, calculator);

        boolean stoped = parkingSessionService.stop(vehicle);

        verifyAll(vehicle, parkingSession, calculator);

        assertThat(stoped).isEqualTo(result);
        assertThat(endCapture.hasCaptured()).isTrue();

        Instant sesseionEnd = endCapture.getValue();
        assertThat(sesseionEnd).isAfterOrEqualTo(requestTime).isBeforeOrEqualTo(now());
    }

}
