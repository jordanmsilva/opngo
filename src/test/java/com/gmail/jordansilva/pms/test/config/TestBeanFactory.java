package com.gmail.jordansilva.pms.test.config;

import static java.util.logging.Level.INFO;
import static java.util.logging.Logger.getLogger;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.ext.ContextResolver;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientProperties;
import org.glassfish.jersey.logging.LoggingFeature;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.fasterxml.jackson.databind.ObjectMapper;

@Configuration
public class TestBeanFactory {

    @Bean
    public Client client(ContextResolver<ObjectMapper> objectMapperProvider) {
        ClientConfig clientConfig = new ClientConfig();
        clientConfig.property(ClientProperties.READ_TIMEOUT, 6000000); // ms
        clientConfig.property(ClientProperties.CONNECT_TIMEOUT, 60000); // ms

        Client client = ClientBuilder.newClient(clientConfig);

        LoggingFeature logger = new LoggingFeature(getLogger(getClass().getName()), INFO,
                LoggingFeature.Verbosity.PAYLOAD_TEXT, 8192);
        client.register(logger);
        client.register(objectMapperProvider);

        return client;
    }
}
