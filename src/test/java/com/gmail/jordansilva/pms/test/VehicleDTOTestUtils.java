package com.gmail.jordansilva.pms.test;

import com.gmail.jordansilva.pms.application.model.VehicleDTO;

public final class VehicleDTOTestUtils {

    public static final VehicleDTO VEHICLE_OK = new VehicleDTO("123ABC");
    public static final VehicleDTO VEHICLE_NO_OWNER = new VehicleDTO("123NOW");
    public static final VehicleDTO VEHICLE_DOESNT_EXIST = new VehicleDTO("DONT_EXIST");
    public static final VehicleDTO VEHICLE_WITHOUT_PLATE_NUMBER = new VehicleDTO("   ");

    private VehicleDTOTestUtils() {
    }
}
