package com.gmail.jordansilva.pms.test;

public final class ParkingLotTestUtils {

    public static final Long PARKING_LOT_ID_ACTIVE = 1l;
    public static final Long PARKING_LOT_ID_INACTIVE = 3l;
    public static final Long PARKING_LOT_ID_DOESNT_EXIST = Long.MAX_VALUE;

    private ParkingLotTestUtils() {
    }
}
