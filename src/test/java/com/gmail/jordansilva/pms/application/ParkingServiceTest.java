package com.gmail.jordansilva.pms.application;

import static com.gmail.jordansilva.pms.test.ParkingLotTestUtils.PARKING_LOT_ID_ACTIVE;
import static com.gmail.jordansilva.pms.test.ParkingLotTestUtils.PARKING_LOT_ID_DOESNT_EXIST;
import static com.gmail.jordansilva.pms.test.ParkingLotTestUtils.PARKING_LOT_ID_INACTIVE;
import static com.gmail.jordansilva.pms.test.VehicleDTOTestUtils.VEHICLE_DOESNT_EXIST;
import static com.gmail.jordansilva.pms.test.VehicleDTOTestUtils.VEHICLE_NO_OWNER;
import static com.gmail.jordansilva.pms.test.VehicleDTOTestUtils.VEHICLE_OK;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.NONE;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.gmail.jordansilva.pms.application.model.VehicleDTO;
import com.gmail.jordansilva.pms.domain.asset.model.ParkingSession;
import com.gmail.jordansilva.pms.domain.user.model.Vehicle;
import com.gmail.jordansilva.pms.infrastructure.repositories.ParkingSessionRepository;

@Transactional
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = NONE)
public class ParkingServiceTest {

    @Inject
    private ParkingService parkingService;

    @Inject
    private ParkingSessionRepository parkingSessionRepository;

    @Value("${parking-price-calculator.standard.unitPrice}")
    private double unitPrice;

    @Test
    public void startParkingShouldStartSessionWhenVehicleUserAndParkingLotAreOk() {
        testStartParkingSuccess(VEHICLE_OK, PARKING_LOT_ID_ACTIVE);
    }

    @Test
    public void startParkingShouldNotStartSessionWhenParkingLotDesntExist() {
        testStartParkingFails(VEHICLE_OK, PARKING_LOT_ID_DOESNT_EXIST);
    }

    @Test
    public void startParkingShouldNotStartSessionWhenParkingLotIsNotActive() {
        testStartParkingFails(VEHICLE_OK, PARKING_LOT_ID_INACTIVE);
    }

    @Test
    public void startParkingShouldNotStartSessionWhenVehicleDoesNotExist() {
        testStartParkingFails(VEHICLE_DOESNT_EXIST, PARKING_LOT_ID_ACTIVE);
    }

    @Test
    public void startParkingShouldNotStartSessionWhenVehicleHasNoOwner() {
        testStartParkingFails(VEHICLE_NO_OWNER, PARKING_LOT_ID_ACTIVE);
    }

    @Test
    public void stopParkingShouldStopParkingWhenVehicleHasActiveParking() throws InterruptedException {
        VehicleDTO vehicleDTO = VEHICLE_OK;

        testStartParkingSuccess(vehicleDTO, PARKING_LOT_ID_ACTIVE);

        Vehicle vehicle = new Vehicle(vehicleDTO.getLicensePlateNumber());
        ParkingSession runningSession = parkingSessionRepository.findOneByVehicleAndEndIsNull(vehicle);

        // Should wait so it will charge 1 unit
        TimeUnit.SECONDS.sleep(1);

        testStopParkingSuccess(vehicleDTO);

        ParkingSession stoppedSession = parkingSessionRepository.findById(runningSession.getId()).get();
        assertThat(stoppedSession.getPrice()).isEqualTo(unitPrice);
    }

    @Test
    public void stopParkingShouldNotStopParkingWhenVehicleHasNoActiveParking() {
        VehicleDTO vehicleDTO = VEHICLE_OK;
        Vehicle vehicle = new Vehicle(vehicleDTO.getLicensePlateNumber());

        assertThat(parkingSessionRepository.findOneByVehicleAndEndIsNull(vehicle)).isNull();
        testStopParkingFails(vehicleDTO);
    }

    @Test
    public void stopParkingShouldNotStopParkingWhenVehicleIsNotRegistered() {
        VehicleDTO vehicleDTO = VEHICLE_DOESNT_EXIST;
        testStopParkingFails(vehicleDTO);
    }

    private void testStartParkingSuccess(VehicleDTO vehicle, long parkingLotId) {
        testStartParking(vehicle, parkingLotId, true);
    }

    private void testStartParkingFails(VehicleDTO vehicle, long parkingLotId) {
        testStartParking(vehicle, parkingLotId, false);
    }

    private void testStartParking(VehicleDTO vehicle, long parkingLotId, boolean createSession) {
        long countBeforeRequest = parkingSessionRepository.count();
        long expectedCount = createSession ? countBeforeRequest + 1 : countBeforeRequest;

        boolean sessionStarted = parkingService.startParking(vehicle, parkingLotId);

        assertThat(sessionStarted).isEqualTo(createSession);

        long countAfterRequest = parkingSessionRepository.count();
        assertThat(countAfterRequest).isEqualTo(expectedCount);
    }

    private void testStopParkingSuccess(VehicleDTO vehicle) {
        testStopParking(vehicle, true);
    }

    private void testStopParkingFails(VehicleDTO vehicle) {
        testStopParking(vehicle, false);
    }

    private void testStopParking(VehicleDTO vehicleDTO, boolean stopped) {
        boolean sessionStopped = parkingService.stopParking(vehicleDTO);
        assertThat(sessionStopped).isEqualTo(stopped);
    }
}
