package com.gmail.jordansilva.pms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OPnGOApplication {

    public static void main(String[] args) {
        SpringApplication.run(OPnGOApplication.class, args);
    }
}
