package com.gmail.jordansilva.pms.infrastructure.repositories;

import org.springframework.data.repository.CrudRepository;

import com.gmail.jordansilva.pms.domain.user.model.User;

public interface UserRepository extends CrudRepository<User, Long> {
}
