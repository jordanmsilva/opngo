package com.gmail.jordansilva.pms.infrastructure.domain.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

@Data
@Configuration
@ConfigurationProperties(prefix = "parking-price-calculator.standard")
public class StandardParkingPriceCalculatorConfig {

    private double unitPrice;
}
