package com.gmail.jordansilva.pms.infrastructure.repositories;

import org.springframework.data.repository.CrudRepository;

import com.gmail.jordansilva.pms.domain.asset.model.ParkingSession;
import com.gmail.jordansilva.pms.domain.user.model.Vehicle;

public interface ParkingSessionRepository extends CrudRepository<ParkingSession, Long> {

    public ParkingSession findOneByVehicleAndEndIsNull(Vehicle vehicle);
}
