package com.gmail.jordansilva.pms.infrastructure.repositories;

import org.springframework.data.repository.CrudRepository;

import com.gmail.jordansilva.pms.domain.asset.model.ParkingLot;

public interface ParkingLotRepository extends CrudRepository<ParkingLot, Long> {
}
