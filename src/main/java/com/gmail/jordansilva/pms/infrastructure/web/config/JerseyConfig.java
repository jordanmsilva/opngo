package com.gmail.jordansilva.pms.infrastructure.web.config;

import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

@Component
public class JerseyConfig extends ResourceConfig {

    public JerseyConfig() {
        packages("com.gmail.jordansilva.pms");

        register(JacksonFeature.class);
    }
}
