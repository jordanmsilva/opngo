package com.gmail.jordansilva.pms.web.rest;

import static java.lang.String.format;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.FORBIDDEN;
import static javax.ws.rs.core.Response.Status.OK;
import static org.apache.commons.lang3.StringUtils.isBlank;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.gmail.jordansilva.pms.application.ParkingService;
import com.gmail.jordansilva.pms.application.model.VehicleDTO;
import com.gmail.jordansilva.pms.web.rest.model.ParkingStatus;

@Path("pms/v1/")
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
public class PmsResource {

    @Inject
    private ParkingService parkingService;

    @POST
    @Path("assets/{parkingLotId}/sessions")
    public Response startSession(@PathParam("parkingLotId") Long parkingLotId, VehicleDTO vehicle) {
        if (vehicle == null || isBlank(vehicle.getLicensePlateNumber())) {
            return buildBadRequest("License plate number is required");
        }

        boolean success = parkingService.startParking(vehicle, parkingLotId);
        return success ? buildOk() : buildForbidden();
    }

    @POST
    @Path("assets/{parkingLotId}/vehicle/{licencePlateNumber}/session")
    public Response stopSession(@PathParam("licencePlateNumber") String licencePlateNumber,
            ParkingStatus parkingStatus) {

        if (parkingStatus == null || parkingStatus.getStatus() != ParkingStatus.Status.STOPPED) {
            return buildBadRequest(format("Invalid status. Status must be %s", ParkingStatus.Status.STOPPED));
        }

        VehicleDTO vehicle = new VehicleDTO(licencePlateNumber);

        boolean success = parkingService.stopParking(vehicle);
        return success ? buildOk() : buildForbidden();
    }

    private Response buildBadRequest(Object entity) {
        return buildResponse(BAD_REQUEST, entity);
    }

    private Response buildOk() {
        return buildResponse(OK, null);
    }

    private Response buildForbidden() {
        return buildResponse(FORBIDDEN, null);
    }

    private Response buildResponse(Status status, Object entity) {
        return Response.status(status).entity(entity).build();
    }
}
