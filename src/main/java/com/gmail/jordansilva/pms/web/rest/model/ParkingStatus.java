package com.gmail.jordansilva.pms.web.rest.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ParkingStatus {

    public enum Status {
        STOPPED
    }

    private Status status;
}
