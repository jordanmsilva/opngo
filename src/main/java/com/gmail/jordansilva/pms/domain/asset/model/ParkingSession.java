package com.gmail.jordansilva.pms.domain.asset.model;

import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.gmail.jordansilva.pms.domain.user.model.Vehicle;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Entity
@NoArgsConstructor
@RequiredArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class ParkingSession {

    @Id
    @GeneratedValue
    @EqualsAndHashCode.Include
    private Long id;

    @NonNull
    @ManyToOne(optional = false)
    private Vehicle vehicle;

    @NonNull
    @Column(nullable = false)
    private Instant start;

    @Setter
    private Instant end;

    @Setter
    private Double price;
}
