package com.gmail.jordansilva.pms.domain.user.model;

import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.EqualsAndHashCode;

@Entity
@EqualsAndHashCode
public class FinancialTransaction {

    @Id
    @GeneratedValue
    @EqualsAndHashCode.Exclude
    private Long id;
    
    @Column(nullable = false)
    private Instant date;
    
    @Column(nullable = false)
    private double amount;
    
    @ManyToOne(optional = false)
    private Wallet wallet;
}
