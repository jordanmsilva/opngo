package com.gmail.jordansilva.pms.domain.pricing;

import java.time.Instant;

public interface ParkingPriceCalculator {

    public double calculate(Instant start, Instant end);
}
