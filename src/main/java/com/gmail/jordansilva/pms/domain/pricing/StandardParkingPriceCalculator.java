package com.gmail.jordansilva.pms.domain.pricing;

import java.time.Duration;
import java.time.Instant;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.gmail.jordansilva.pms.infrastructure.domain.config.StandardParkingPriceCalculatorConfig;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor(onConstructor_ = { @Inject })
public class StandardParkingPriceCalculator implements ParkingPriceCalculator {

    private static final long TIME_UNIT_IN_SECONDS = 15 * 60;

    private StandardParkingPriceCalculatorConfig config;

    @Override
    public double calculate(Instant start, Instant end) {
        long totalUnits = getParkingTimeInUnits(start, end);
        return totalUnits * config.getUnitPrice();
    }

    private long getParkingTimeInUnits(Instant start, Instant end) {
        long secondsParked = Duration.between(start, end).getSeconds();

        long fullUnits = secondsParked / TIME_UNIT_IN_SECONDS;
        long incompletedUnits = (secondsParked % TIME_UNIT_IN_SECONDS) > 0 ? 1 : 0;

        return fullUnits + incompletedUnits;
    }

}
