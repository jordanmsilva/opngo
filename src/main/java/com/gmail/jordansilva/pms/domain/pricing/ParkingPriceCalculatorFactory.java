package com.gmail.jordansilva.pms.domain.pricing;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor(onConstructor_ = { @Inject })
public class ParkingPriceCalculatorFactory {

    @Inject // We can inject as it is stateless
    private StandardParkingPriceCalculator standardParkingPriceCalculator;

    /*
     * This should be changed when new Calculators are created.
     */
    public ParkingPriceCalculator getCalculator() {
        return standardParkingPriceCalculator;
    }
}
