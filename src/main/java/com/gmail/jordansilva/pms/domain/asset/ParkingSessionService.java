package com.gmail.jordansilva.pms.domain.asset;

import static java.time.Instant.now;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.gmail.jordansilva.pms.domain.asset.model.ParkingSession;
import com.gmail.jordansilva.pms.domain.pricing.ParkingPriceCalculator;
import com.gmail.jordansilva.pms.domain.pricing.ParkingPriceCalculatorFactory;
import com.gmail.jordansilva.pms.domain.user.model.Vehicle;
import com.gmail.jordansilva.pms.infrastructure.repositories.ParkingSessionRepository;

import lombok.AllArgsConstructor;
import lombok.NonNull;

@Service
@AllArgsConstructor(onConstructor_ = { @Inject })
public class ParkingSessionService {

    private ParkingPriceCalculatorFactory priceCalculatorFactory;

    private ParkingSessionRepository parkingSessionRepository;

    public ParkingSession start(@NonNull Vehicle vehicle) {

        if (vehicle.getOwner() == null) {
            return null;
        }

        ParkingSession parkingSession = new ParkingSession(vehicle, now());
        return parkingSessionRepository.save(parkingSession);
    }

    public boolean stop(@NonNull Vehicle vehicle) {
        boolean stopped = false;

        ParkingSession parkingSession = parkingSessionRepository.findOneByVehicleAndEndIsNull(vehicle);

        if (parkingSession != null) {
            parkingSession.setEnd(now());
            parkingSession.setPrice(calculateTotalPrice(parkingSession));

            try {
                parkingSessionRepository.save(parkingSession);
                stopped = true;
            } catch (Exception e) {
                // TODO: handle exception - future development
            }
        }

        return stopped;
    }

    private Double calculateTotalPrice(ParkingSession parkingSession) {
        ParkingPriceCalculator calculator = priceCalculatorFactory.getCalculator();
        return calculator.calculate(parkingSession.getStart(), parkingSession.getEnd());
    }
}
