package com.gmail.jordansilva.pms.domain.user.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@RequiredArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Vehicle {

    @Id
    @NonNull
    @EqualsAndHashCode.Include
    private String plateNumber;

    @ManyToOne
    @Setter
    private User owner;

    @Setter
    private String desciption;

}
