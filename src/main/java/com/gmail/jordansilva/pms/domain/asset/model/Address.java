package com.gmail.jordansilva.pms.domain.asset.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.EqualsAndHashCode;

@Entity
@EqualsAndHashCode
@Table(uniqueConstraints = { @UniqueConstraint(columnNames = { "street", "number", "city", "country", "postalCode" }) })
public class Address {

    @Id
    @GeneratedValue
    @EqualsAndHashCode.Exclude
    private Long id;
    
    private String street;
    
    private String number;
    
    private String city;
    
    private String country;
    
    private String postalCode;
}
