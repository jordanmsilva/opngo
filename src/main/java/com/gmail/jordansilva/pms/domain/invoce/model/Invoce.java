package com.gmail.jordansilva.pms.domain.invoce.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.gmail.jordansilva.pms.domain.asset.model.ParkingSession;
import com.gmail.jordansilva.pms.domain.user.model.User;

@Entity
public class Invoce {

    @Id
    @GeneratedValue
    private Long id;
    
    @ManyToOne
    @JoinColumn(nullable = false)
    private User user;
    
    @ManyToOne(optional = false)
    @JoinColumn(unique = true)
    private ParkingSession session;
}
