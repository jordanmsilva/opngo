package com.gmail.jordansilva.pms.domain.asset.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import lombok.EqualsAndHashCode;
import lombok.Getter;

@Entity
@Getter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class ParkingLot {

    @Id
    @GeneratedValue
    private Long id;

    @EqualsAndHashCode.Include
    @OneToOne(optional = false)
    private Address address;

    @Column(nullable = false)
    private boolean active;
}
