package com.gmail.jordansilva.pms.application;

import java.util.Optional;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.gmail.jordansilva.pms.application.model.VehicleDTO;
import com.gmail.jordansilva.pms.domain.asset.ParkingSessionService;
import com.gmail.jordansilva.pms.domain.asset.model.ParkingLot;
import com.gmail.jordansilva.pms.domain.user.model.Vehicle;
import com.gmail.jordansilva.pms.infrastructure.repositories.ParkingLotRepository;
import com.gmail.jordansilva.pms.infrastructure.repositories.VehicleRepository;

import lombok.NonNull;

@Service
public class ParkingService {

    @Inject
    private VehicleRepository vehicleRepository;

    @Inject
    private ParkingLotRepository parkingLotRepository;

    @Inject
    private ParkingSessionService parkingSessionService;

    public boolean startParking(@NonNull VehicleDTO vehicleDTO, long parkingLotId) {
        Optional<ParkingLot> parkingLot = parkingLotRepository.findById(parkingLotId);
        Optional<Vehicle> vehicle = vehicleRepository.findById(vehicleDTO.getLicensePlateNumber());

        if (!parkingLot.isPresent() || !parkingLot.get().isActive() || !vehicle.isPresent()) {
            return false;
        }

        return parkingSessionService.start(vehicle.get()) != null;
    }

    public boolean stopParking(@NonNull VehicleDTO vehicleDTO) {
        Optional<Vehicle> vehicle = vehicleRepository.findById(vehicleDTO.getLicensePlateNumber());

        if (!vehicle.isPresent()) {
            return false;
        }

        return parkingSessionService.stop(vehicle.get());
    }
}
