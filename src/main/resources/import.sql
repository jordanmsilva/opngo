-- Data that will be loaded to Application on startup

-- Address
INSERT INTO address (id, country, city, street, number, postal_code) VALUES (1, 'EST', 'Tallinn', 'Narva mnt', '34A', '11234');
INSERT INTO address (id, country, city, street, number, postal_code) VALUES (2, 'EST', 'Tallinn', 'Narva mnt', '1', '11234');
INSERT INTO address (id, country, city, street, number, postal_code) VALUES (3, 'EST', 'Tallinn', 'Lenu tee', '64', '11235');

-- Parking Lot
INSERT INTO parking_lot (id, address_id, active) VALUES (1, 1, true);
INSERT INTO parking_lot (id, address_id, active) VALUES (2, 2, true);
INSERT INTO parking_lot (id, address_id, active) VALUES (3, 3, false);

-- Wellet
INSERT INTO wallet (id, balance) VALUES (1, 110);

-- User
INSERT INTO user (id, email, password, wallet_id) VALUES (1, 'jhon@mail.com', '123455', 1);

-- Vehicle
INSERT INTO vehicle (plate_number, desciption, owner_id) VALUES ('123ABC', 'Mine', 1);
INSERT INTO vehicle (plate_number, desciption, owner_id) VALUES ('123NOW', null, null);